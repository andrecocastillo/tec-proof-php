<div id="panel">
    <div>
      ID : <?php echo $costumerData->id ?> </br>
      First name : <?php echo $costumerData->firstName ?> </br>
      Last name : <?php echo $costumerData->lastName ?> </br>
    </div>
    <table id="table-data" class="table table-striped">
      <tr>
        <td><b>Ref</b></td>
        <td><b>Product</b></td>
        <td><b>Quiantity</b></td>
        <td><b>Price</b></td>
        <td><b>Total</b></td>
      </tr>
      <?php foreach($items as $vl){ ?>
        <tr>
          <td><?php echo $vl->id; ?></td>
          <td><?php echo $vl->name; ?></td>
          <td><?php echo $vl->quantity; ?></td>
          <td>$ <?php echo number_format($vl->price); ?></td>
          <td>$ <?php echo number_format($vl->priceWithQuiantity); ?></td>
        </tr>
      <?php } ?>
    </table>
    <div>
      SubTotal : $<?php echo $subTotal ?> </br>
      Total : $<?php echo $total ?> </br>
    </div>   
</div>