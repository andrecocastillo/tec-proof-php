<?php
error_reporting(E_ALL ^ E_NOTICE);  
include('./constants/genetal.php');
include('./controller/userInformation.php');
include('./controller/store.php');
include ('./lib/curl.php');
/*
  CONTROLLER
  This file functions as a controller.
*/
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<div id="general-panel">
  
  <a href="index.php?option=1"> Question 1 - List Users</a> | 
  <a href="index.php?option=2&order[]=first_name|desc&order[]=account_id|asc"> Question 2 - List Users order by</a> | 
  <a href="index.php?option=3"> Question 3 - Store</a>

  <br>
  <?php 
  if(@$_GET['option'] == 1){ 
    $UserInformation = new UserInformationController();
    $UserInformation->userInformation(); 
  }
  if(@$_GET['option'] == 2){ 
    $UserInformation = new UserInformationController();
    $UserInformation->userInformationOrderBy($_GET['order']);
  }
  if(@$_GET['option'] == 3){
    $StoreController = new StoreController();
    $StoreController->purchaseUser(); 
  }
  ?>
</div>

<style>
  #general-panel{
    width: 90%;
    margin: auto;
    margin-top: 10px;
  }

  #users-booking{
    width: 100%;
    border-collapse: collapse;
  }

  #users-booking table, #users-booking th, #users-booking td {
    border: 1px solid;
    border-spacing: 0;
    padding: 0px;
  }
  .booking-table{
    margin: 5px;
    width: 90%;
    margin: auto;
  }
  #panel{
    width: 80%;
    margin: auto;
    margin-top: 20px;
  }
  #panel table{
    width: 100%;
  }
  #panel #table-data{
    width: 100%;
    border: 1px solid;
    border-collapse: collapse;
  }
  
  #table-data, #table-data th, #table-data td {
    border: 1px solid;
    border-spacing: 0;
    padding: 0px;
  }
</style>