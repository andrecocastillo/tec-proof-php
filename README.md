# Technical proof

Please ensure that you understand that this is a test project and is not intended for production use. The purpose of this project is to serve as a technical proof of concept.

## Installation

- Clone the project from the Git repository.
- Place the PHP files in your "www" folder if you are using Apache version 4.4 or higher.
- Ensure that your PHP version is compatible with this project; it works with PHP 7.0 or later.

## Demo

In this repo you can find the file video.mp4 

## Developer

Andres Castillo - Software Developer

## Mockup
I'm usign apimocha.com for build a movkup of shipping.
curl --request GET 'https://apimocha.com/shipping-provider/rate' --header 'Content-Type: application/json'

## License

[MIT](https://choosealicense.com/licenses/mit/)