<?php

include('./models/store.php');

class StoreController{

  function purchaseUser(){
    $Store = new Store();
    $Store->setInformationCostumer(123, 'Andres', 'Castillo', [
      [
        'line_1'=>' my dir 1',
        'line_2'=>'my dir 2',
        'city'=>'Bogota',
        'state'=>'Cundinamarca',
        'zip'=>'0001'
      ],
      [
        'line_1'=>' my dir 1',
        'line_2'=>'my dir 2',
        'city'=>'Bogota',
        'state'=>'Cundinamarca',
        'zip'=>'0001'
      ]
    ]);

    $Store->appedItem(1, 'Dog toy', 1, 9000);
    $Store->appedItem(2, 'Cat toy', 2, 8200);
    $Store->appedItem(3, 'Elephant toy', 3, 7500);

    $Store->calculateSubtotal();
    $Store->calculateTotal();

    $costumerData = $Store->costumer();
    $items = $Store->items();
    $subTotal = number_format($Store->calculateSubtotal()) ;
    $total = number_format($Store->calculateTotal());

    include('./view/purchase.php');
  }
}