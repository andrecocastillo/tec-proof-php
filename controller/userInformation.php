<?php 
/*
  MODEL
  This file functions as a model.
*/

include('./repository/data.php');
include('./lib/orderData.php');

class UserInformationController{

  /*
    userInformation
    This function displays information about the selected users and bookings.
  */
  function userInformation(){
    $userData = [];

    foreach(USER_DATA as $user){
      /*
        In this function, we have the ability to modify the information and implement custom logic. 
      */
      $user['start_time'] = $user['guest_booking']['start_time'];
      $user['end_time'] = $user['guest_booking']['end_time'];
      $userData[] = $user;
    }

    include('./view/listUser.php');
  }


  /*
    userInformationOrderBy
    This function arranges the table's information based on the criteria obtained from the URL.
  */
  function userInformationOrderBy($order){
    $userData = [];

    foreach(USER_DATA as $user){
      /*
        In this function, we have the ability to modify the information and implement custom logic. 
      */
      $user['start_time'] = $user['guest_booking']['start_time'];
      $user['end_time'] = $user['guest_booking']['end_time'];
      $userData[] = $user;
    }

    $orderData = new OrderData($userData, $order);
    $userData = $orderData->getData();

    include('./view/listUser.php');
  }
}
?>