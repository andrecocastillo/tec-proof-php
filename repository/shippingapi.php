<?php

class ShippingApi{
  
  function getRate(){
    $url = 'https://apimocha.com/shipping-provider/rate';
    $rate = Curl::call('GET', $url, [], []);
    $rate = $rate['rate'] ? $rate['rate'] : 0;
    return $rate;
  }

}
