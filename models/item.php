<?php

class Item{
  private $id;
  private $name;
  private $quantity;
  private $price;
  private $priceWithQuiantity = 0;

  public function __get($name) {
    switch($name){
      case 'id';
        return $this->id;
      break;
      case 'name';
        return $this->name;
      break;
      case 'quantity';
        return $this->quantity;
      break;
      case 'price';
        return $this->price;
      break;
      case 'priceWithtax';
        return $this->priceWithtax;
      break;
      case 'priceWithQuiantity';
      return $this->priceWithQuiantity;
      break;
    }
  }

  public function __set($name, $value) {
    switch($name){
      case 'id';
        $this->id = $value;
      break;
      case 'name';
      $this->name = $value;
      break;
      case 'quantity';
      $this->quantity = $value;
      break;
      case 'price';
      $this->price = $value;
      break;
    }
  }

  public function priceWithQuantity(){
    $this->priceWithQuiantity = $this->quantity * $this->price;
  }
}