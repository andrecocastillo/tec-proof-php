<?php

include('./models/address.php');

class Costumer extends Address{

  private $id = '';
  private $firstName = '';
  private $lastName = '';

  public function __get($name) {
    switch($name){
      case 'id';
        return $this->id;
      break;
      case 'firstName';
        return $this->firstName;
      break;
      case 'lastName';
        return  $this->lastName;
      break;
    }
  }

  public function __set($name, $value) {
    switch($name){
      case 'id';
      $this->id = $value;
      break;
      case 'firstName';
        $this->firstName = $value;
      break;
      case 'lastName';
      $this->lastName = $value;
      break;
      case 'addresses';
        $this->appendAddres($value['line_1'], $value['line_2'], $value['city'], $value['state'], $value['zip']);
      break;
    }
  }

}

