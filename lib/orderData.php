<?php



class OrderData{

  private $data = [];
  private $order = [];

  function __construct($data, $order){
    $this->data = $data;
    $this->order = $order;

    $dataGrouped = $this->groupValues($this->data);
    $orderValues = $this->orderValues($dataGrouped);
    $this->data = $this->composeTable($orderValues);
  }

  private function orderValues($dataGrouped){
    $order = explode('|', $this->order[0]);
    $direction = $order[1];

    $column = array_column($dataGrouped, 'value');
    $direction = ($direction == 'desc') ? SORT_DESC : SORT_ASC;
    array_multisort($column, $direction, $dataGrouped);  
    
    $finalOrder = [];
    foreach($dataGrouped as $vl){
      $order = explode('|', $this->order[1]);
      $direction = $order[1];
      $column = array_column($vl['row'], 'value');
      $direction = ($direction == 'desc') ? SORT_DESC : SORT_ASC;
      array_multisort($column, $direction, $vl['row']);

      $finalOrder[] = $vl;
    }
    return $finalOrder;
  }

  private function groupValues(&$data){
    
    if(!empty(reset($data)[0]['row'])){
      $pibot = explode('|', $this->order[1])[0];
      foreach($data as $cl => $vl){
        $tmpData = [];
        
        foreach($vl as $vll){
          $vll['value'] = $this->defineValuePibot($vll['row'], $pibot);
          $tmpData[] = $vll;
        }

        $data[$cl] = [
          'value'=> $data[$cl][0]['value'],
          'row'=> $tmpData
        ];
      }
    }else if(empty(reset($data)[0]['row'])){

      $tmpData = [];
      foreach($data as $cl => $vl){
        $pibot = explode('|', $this->order[0])[0];
        $keyValue = $this->defineValuePibot($vl, $pibot);

        $tmpData[$keyValue][] = [
          'value'=> $vl[$pibot],
          'row'=> $vl
        ];        
      }

      return $this->groupValues($tmpData);
    }

    return array_values($data);
  }

  private function defineValuePibot($value, $pibot){

    if(in_array($pibot,['booking_number', 'ship_code', 'room_no', 'start_time', 'end_time', 'is_checked_in'])){
      return $value['guest_booking'][0][$pibot];
    }

    if(in_array($pibot,['account_id', 'status_id', 'account_limit', 'allow_charges'])){
      return $value['guest_account'][0][$pibot];
    }
      
    return $value[$pibot];
  }

  private function composeTable($orderValues){

    $table = [];
    foreach($orderValues as $cl => $vl){
      foreach($vl['row'] as $vll){
        $table[] = $vll['row'];
      }
    }

    return $table;
  }

  function getData(){
    return $this->data;
  }

}