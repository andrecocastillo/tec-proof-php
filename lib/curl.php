<?php

class Curl{

  public static function call(string $method, string $url, array $params, array $configs=[]){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      if($method == 'GET'){
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      }else{
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
      }
      $response = curl_exec($ch);
      curl_close ($ch);
      return json_decode($response,true);
  }     
}
